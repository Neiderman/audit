<?php

namespace App\Http\Middleware;

use Closure, Auth;

class valAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->rol->nombre != "Super Administrador") {
            return redirect(route("home"));
        }
        return $next($request);
    }
}
