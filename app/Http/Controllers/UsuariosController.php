<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Rol;
use App\Empresa;

use App\Mail\confirmarRegistro;

use Auth,Validator,Crypt,Mail;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->inv_activa == "si") {
            return $this->validar(Crypt::encrypt(Auth::user()->id));
        }

        $usuarios = User::get();
        return view('usuario.index',compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Rol::get();
        $empresas = Empresa::get();

        return view('usuario.create',compact('roles','empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'inv_activa' => ['required', 'string', 'max:4'],
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            
            'empresa' => ['required_if:rol,"Administrador"', 'nullable', 'string', 'max:255'],
            'usuario' => ['required_if:inv_activa,no', 'nullable', 'string', 'max:255', 'unique:users'],
            'password' => ['required_if:inv_activa,no', 'nullable', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('usuarios.create'))->withErrors($validacion)->withInput();
        }

        $inv_activa = $request->inv_activa;

        $usuario = new User();
        $usuario->nombre = $request->nombre;
        $usuario->email = $request->email;
        $usuario->roles_id = $request->rol;

        if ($usuario->rol->nombre == 'Administrador') {
            $usuario->empresas_id = $request->empresa;
        } else if ($usuario->rol->nombre != 'Super Administrador') {
            $id_empresa = Auth::user()->empresas_id;

            if (is_null($id_empresa)) {
                $id_empresa = $request->empresa;
            }

            $usuario->empresas_id = $id_empresa;
        }

        $usuario->inv_activa = $inv_activa;

        if ($inv_activa == "no") {
            $usuario->usuario = $request->usuario;
            $usuario->password = $request->password;
        }

        $usuario->save();

        $this->enviar_invitacion($usuario->id);

        flash('Se ha creado el usuario.')->success();
        return redirect(route('usuarios.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::findOrFail($id);

        return view('usuario.edit',compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validacion = Validator::make($request->all(), [
            'inv_activa' => ['required', 'string', 'max:4'],
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,id,'.$id],
            
            'usuario' => ['required_if:inv_activa,no', 'nullable', 'string', 'max:255', 'unique:users,id,'.$id],
            'password' => ['required_if:inv_activa,no', 'nullable', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('usuarios.edit'))->withErrors($validacion)->withInput();
        }

        $usuario = new User();
        $usuario->nombre = $request->nombre;
        $usuario->email = $request->email;
        $usuario->roles_id = $request->rol;

        $usuario->inv_activa = $request->inv_activa;

        $usuario->usuario = $request->usuario;
        $usuario->password = $request->password;

        $usuario->save();

        flash('Se ha actualizado el usuario con exito.')->success();
        return redirect(route('usuarios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perfil()
    {
        $usuario = Auth::user();
        return view('usuario.perfil',compact('usuario'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_perfil(Request $request, $id)
    {
        $validacion = Validator::make($request->all(), [
            'usuario' => ['required', 'string', 'max:255', 'unique:users,id,'.$id],
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,id,'.$id],
            'password' => ['nullable','string', 'min:8', 'confirmed'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('perfil'))->withErrors($validacion)->withInput();
        }

        $usuario = User::find(Auth::user()->id);
        $usuario->usuario = $request->usuario;
        $usuario->nombre = $request->nombre;
        $usuario->email = $request->email;

        if ($request->password) {
            $usuario->password = $request->password;
        }

        $usuario->save();

        flash('Se ha actualizado tu perfil.')->success();
        return redirect(route('perfil'));
    }

    public function enviar_invitacion_url(Request $request,$id)
    {
        $this->enviar_invitacion($id);

        flash('Se enviado la invitación.')->success();

        return redirect(route('usuarios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function enviar_invitacion($id)
    {
        $usuario = User::findOrFail($id);

        Mail::to($usuario->email)->send(new confirmarRegistro(route('registro',Crypt::encrypt($usuario->id))));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validar($id)
    {
        $id = Crypt::decrypt($id);
        $usuario = User::findOrFail($id);

        if ($usuario->inv_activa == "no") {
            flash('Ha ocurrido un error, la cuenta ya esta activa.')->warning();
            return redirect(route('login'));
        }

        Auth::login($usuario);

        return view('usuario.continue',compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validar_update(Request $request, $id)
    {
        $validacion = Validator::make($request->all(), [
            'inv_activa' => ['required', 'string', 'max:4'],
            'usuario' => ['required', 'string', 'max:255'],
            'nombre' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validacion->fails()) {
            flash('Revisa los campos marcados en rojo.')->warning();
            return redirect(route('usuarios.continue'))->withErrors($validacion)->withInput();
        }

        $usuario = User::findOrFail($id);
        
        $usuario->usuario = $request->usuario;
        $usuario->nombre = $request->nombre;
        $usuario->password = $request->password;
        $usuario->inv_activa = 'no';


        $usuario->save();

        flash('Se ha completado el registro con exito.')->success();
        return redirect(route('usuarios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function estado(Request $request,$id)
    {
        if (Auth::user()->id == $id) {
            flash('Ha ocurrido un error, el estado de la cuenta no puede ser modificado.')->warning();
            return redirect(route('usuarios.index'));
        }

        $usuario = User::findOrFail($id);
        $estado = 'si';
        
        if ($usuario->activo == 'si') {
            $estado = 'no';
        }


        $usuario->update(['activo' => $estado]);

        return redirect(route('usuarios.index'));
    }
}
