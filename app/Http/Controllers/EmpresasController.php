<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\User;
use Illuminate\Http\Request;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::get();
        $usuarios = User::whereNotNull('empresas_id')->get();
        return view('empresa.index',compact('empresas','usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nit = $request->nit;
        $nombre = $request->nombre;
        $activo = $request->activo;
        $email = $request->email;

        $empresa = new Empresa();
        $empresa->nit = $nit;
        $empresa->nombre = $nombre;
        $empresa->activo = $activo;
        $empresa->email = $email;

        $empresa->save();
        return redirect(route('empresas.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = Empresa::findOrFail($id);
        return view('empresa.edit',compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nit = $request->nit;
        $nombre = $request->nombre;
        $activo = $request->activo;
        $email = $request->email;

        $empresa = Empresa::findOrFail($id);
        $empresa->nit = $nit;
        $empresa->nombre = $nombre;
        $empresa->activo = $activo;
        $empresa->email = $email;

        $empresa->save();
        return redirect(route('empresas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function estado(Request $request,$id)
    {
        $empresa = Empresa::findOrFail($id);
        $estado = 'si';
        
        if ($empresa->activo == 'si') {
            $estado = 'no';
        }

        $empresa->update(['activo' => $estado]);

        return redirect(route('empresas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function usuarios($id)
    {
        $empresa = Empresa::findOrFail($id);
        $usuarios = User::where('empresas_id',$id)->get();

        return view('empresa.usuarios',compact('empresa','usuarios'));
    }
}
