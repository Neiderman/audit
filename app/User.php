<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','activo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Se encarga de retornar el rol del usuario    
     */
    
    public function rol()
    {
        return $this->hasOne('App\Rol','id','roles_id');
    }

    /**
     * Se encarga de retornar el rol del usuario    
     */
    
    public function empresa()
    {
        return $this->hasOne('App\Empresa','id','empresas_id');
    }
}
