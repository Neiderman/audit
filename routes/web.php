<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/register', function() {
	return redirect(route('login'));
})->name('home');

Route::get('/', function() {
	return redirect(route('home'));
})->name('home');

Route::get('/validar/{token}', 'UsuariosController@validar')->name('registro');

/* Para acceder a este middleware deben estar logueados */
Route::group(['middleware' => ['web','auth']], function () {
	Route::get('/inicio', 'HomeController@index')->name('home');
	Route::get('/salir', 'HomeController@salir')->name('salir');
	Route::get('/perfil', 'UsuariosController@perfil')->name('perfil');
	Route::post('/perfil/{id}/update_perfil', 'UsuariosController@update_perfil')->name('update_perfil');

	/* Solo super administradores */
	// Route::group(['middleware' => ['administrador','empresa']], function () {
		Route::resource('empresas', 'EmpresasController');
		Route::get('empresas/estado/{id}', 'EmpresasController@estado')->name('empresa_estado');
		Route::get('empresas/{id}/usuarios', 'EmpresasController@usuarios')->name('empresa_usuarios');

		Route::resource('usuarios', 'UsuariosController');
		Route::get('usuarios/estado/{id}', 'UsuariosController@estado')->name('usuario_estado');
		Route::get('usuarios/{id}/enviar_invitacion', 'UsuariosController@enviar_invitacion_url')->name('enviar_invitacion');
		Route::post('usuarios/{id}/validar_update', 'UsuariosController@validar_update')->name('validar_update');

		Route::resource('proyectos', 'ProyectosController');
		
		Route::resource('formularios', 'FormulariosController');
		Route::get('formularios/{id}/formulario', 'FormulariosController@get_formulario')->name('formulario_cuerpo');
	// });
});

