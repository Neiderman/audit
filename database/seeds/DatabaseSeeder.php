<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$id_admin = DB::table('roles')->insertGetId([
    		'nombre' => 'Super Administrador',
    	]);

    	DB::table('roles')->insert([
    		'nombre' => 'Administrador'
    	]);

    	DB::table('roles')->insert([
    		'nombre' => 'Coordinador'
    	]);

    	DB::table('roles')->insert([
    		'nombre' => 'Supervisor'
    	]);

    	DB::table('roles')->insert([
    		'nombre' => 'Auditor'
    	]);

    	DB::table('users')->insert([
    		'roles_id' => $id_admin,
    		'usuario' => 'p_usuario',
    		'nombre' => 'Primer Usuario',
    		'email' => 'admin@admin.com',
    		'password' => Hash::make('123456')
    	]);
    }
}
