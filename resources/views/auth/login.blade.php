@extends('layouts.auth')

@section('tipo')
Login
@endsection

@section('contenido')
<div class="brand-logo">
    {{-- <img src="{{ asset('admin/images/auditoria.png') }}" alt="logo"> --}}
    <center><strong>Login - Auditorias</strong></center>
</div>

<form class="pt-3" method="POST" action="{{ route('login') }}">
    @csrf
    
    <div class="form-group">
        <input type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Correo electronico" autofocus>

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

    </div>

    <div class="form-group">
        <input type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" id="password" name="password" placeholder="Contraseña" required autocomplete="current-password">

        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

    </div>

    <div class="mt-3">
        <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">Iniciar sesión</button>
    </div>
    <div class="my-2 d-flex justify-content-between align-items-center">
        <div class="form-check">
            <label class="form-check-label text-muted">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                Mantenerme en el sistema
            </label>
        </div>
        <a href="{{ route('password.request') }}" class="auth-link text-black">Olvidaste tu contraseña?</a>
    </div>
</form>
@endsection
