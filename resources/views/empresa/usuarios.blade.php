@extends('layouts.app')

@section('titulo')
Usuarios de empresa
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('empresas.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Empresas&nbsp;/&nbsp;</p></a>
                    <p class="text-muted mb-0 hover-cursor">[ {!! $empresa->nombre !!} ]&nbsp;/&nbsp;</p>
                    <p class="text-primary mb-0 hover-cursor">Usuarios por empresa</p>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('empresas.index') }}';" class="btn btn-secondary mt-2 mt-xl-0">Volver</button>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title">Listado de usuarios</p>
                <div class="table-responsive">
                    <table id="recent-purchases-listing" class="table">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Nombre</th>
                                <th>Correo electronico</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($usuarios as $usuario)
                            <tr>
                                <td>{!! $usuario->usuario !!}</td>
                                <td>{!! $usuario->nombre !!}</td>
                                <td>{!! $usuario->email !!}</td>
                                <td>{!! $usuario->activo == 'si' ? 'Activo' : 'Inactivo' !!}</td>
                                <td>
                                    <div class="btn-group col-sm">
                                        <button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('empresas.edit',$usuario->id) }}';" title="Editar empresa."><i class="mdi mdi-grease-pencil"></i></button>
                                        
                                        @if ($usuario->activo == 'no')
                                        
                                        <button class="btn btn-xs btn-success" onclick="location.href='{{ route('empresa_estado',$usuario->id) }}';" title="Habilitar empresa"><i class="mdi mdi-check"></i></button>
                                        
                                        @else
                                        
                                        <button class="btn btn-xs btn-danger" onclick="location.href='{{ route('empresa_estado',$usuario->id) }}';" title="Inhabilitar empresa"><i class="mdi mdi-close"></i></button>
                                        
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#recent-purchases-listing').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });
    });
</script>
@endsection
