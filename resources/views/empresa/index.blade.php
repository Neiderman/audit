@extends('layouts.app')

@section('titulo')
Inicio
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>Gestión de empresas</h2>

                    <div class="d-flex">
                        <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Empresas&nbsp;/&nbsp;</p>
                    </div>
                </div>

            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('empresas.create') }}';" class="btn btn-primary mt-2 mt-xl-0">Crear empresa</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body dashboard-tabs p-0">
                <div class="tab-content py-0 px-0">
                    <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                        <div class="d-flex flex-wrap justify-content-xl-between">
                            <div class="d-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                                <i class="mdi mdi mdi-counter mr-3 icon-lg text-danger"></i>
                                <div class="d-flex flex-column justify-content-around">
                                    <small class="mb-1 text-muted">Cantidad de empresas creadas</small>
                                    <h5 class="mr-2 mb-0">{{ $empresas->count() }}</h5>
                                </div>
                            </div>
                            <div class="d-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                                <i class="mdi mdi mdi-account mr-3 icon-lg text-success"></i>
                                <div class="d-flex flex-column justify-content-around">
                                    <small class="mb-1 text-muted">Cantidad de usuarios en empresas</small>
                                    <h5 class="mr-2 mb-0">{!! $usuarios->count() !!}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title">Listado de empresas</p>
                <div class="table-responsive">
                    <table id="recent-purchases-listing" class="table">
                        <thead>
                            <tr>
                                <th>Nit</th>
                                <th>Nombre</th>
                                <th>Correo electronico</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($empresas as $empresa)
                            <tr>
                                <td>{!! $empresa->nit !!}</td>
                                <td>{!! $empresa->nombre !!}</td>
                                <td>{!! $empresa->email !!}</td>
                                <td>{!! $empresa->activo == 'si' ? 'Activo' : 'Inactivo' !!}</td>
                                <td>
                                    <div class="btn-group col-sm">
                                        <button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('empresas.edit',$empresa->id) }}';" title="Editar empresa."><i class="mdi mdi-grease-pencil"></i></button>
                                        
                                        @if ($empresa->activo == 'no')
                                        
                                        <button class="btn btn-xs btn-success" onclick="location.href='{{ route('empresa_estado',$empresa->id) }}';" title="Habilitar empresa"><i class="mdi mdi-check"></i></button>
                                        
                                        @else
                                        
                                        <button class="btn btn-xs btn-primary" type="button" onclick="location.href='{{ route('empresa_usuarios',$empresa->id) }}';" title="Ver usuarios."><i class="mdi mdi-account-multiple"></i></button>
                                        <button class="btn btn-xs btn-danger" onclick="location.href='{{ route('empresa_estado',$empresa->id) }}';" title="Inhabilitar empresa"><i class="mdi mdi-close"></i></button>
                                        
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#recent-purchases-listing').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });
    });
</script>
@endsection
