@extends('layouts.app')

@section('titulo')
Crear empresa
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('empresas.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Empresas&nbsp;/&nbsp;</p></a>
                    <p class="text-primary mb-0 hover-cursor">Crear empresa</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Información general de la empresa</h4>
                <form method="POST" action="{{ route('empresas.store') }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nit">Nit</label>
                                <input type="text" class="form-control" id="nit" name="nit" value="" placeholder="Nit">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" value="" placeholder="Nombre completo">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Correo electronico</label>
                                <input type="email" class="form-control" id="email" name="email" value="" placeholder="Correo electronico">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="activo">Estado</label>
                                <select class="form-control" id="activo" name="activo">
                                    <option value="si">Activo</option>
                                    <option value="no">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="btn-group">
                        <button type="button" onclick="location.href='{{ route('empresas.index') }}';" class="btn btn-secondary">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Crear empresa</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
