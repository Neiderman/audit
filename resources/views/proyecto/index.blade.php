@extends('layouts.app')

@section('titulo')
Proyectos
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor">&nbsp;/&nbsp;</i></a>
                    <p class="text-primary mb-0 hover-cursor">Proyectos</p>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('proyectos.create') }}';" class="btn btn-primary mt-2 mt-xl-0">Crear proyecto</button>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title">Listado de proyectos</p>
                <div class="table-responsive">
                    <table id="recent-purchases-listing" class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Fecha de creación</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($proyectos as $proyecto)
                            <tr>
                                <td>{!! $proyecto->nombre !!}</td>
                                <td>{!! $proyecto->email !!}</td>
                                <td>
                                    {!! $proyecto->activo == 'si' ? 'Activo' : 'Inactivo' !!}
                                </td>
                                <td>
                                    <div class="btn-group col-sm">

                                        <button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('usuarios.edit',$proyecto->id) }}';" title="Editar empresa."><i class="mdi mdi-grease-pencil"></i></button>

                                        @if ($proyecto->activo == 'no')
                                        
                                        {{----}}<button class="btn btn-xs btn-success" onclick="location.href='{{ route('usuario_estado',$proyecto->id) }}';" title="Habilitar empresa"><i class="mdi mdi-check"></i></button>
                                        
                                        @else
                                        
                                        {{----}}<button class="btn btn-xs btn-danger" onclick="location.href='{{ route('usuario_estado',$proyecto->id) }}';" title="Inhabilitar empresa"><i class="mdi mdi-close"></i></button>
                                        
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#recent-purchases-listing').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });
    });
</script>
@endsection
