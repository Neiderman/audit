@extends('layouts.app')

@section('titulo')
Crear formulario
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('usuarios.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Formularios&nbsp;/&nbsp;</p></a>
                    <p class="text-primary mb-0 hover-cursor">Crear formulario</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('proyectos.store') }}">
                    @csrf

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="estado" class="col-md-4 col-form-label text-md-right">Estado</label>

                                <select id="estado" type="text" class="form-control @error('estado') is-invalid @enderror" name="estado" required autofocus>
                                    <option value="0">Inactivo</option>
                                    <option value="1">Activo</option>
                                </select>

                                @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="rol">Supervisor</label>
                                <select class="form-control" id="supervisor" name="supervisor" required>
                                    <option value="" selected>Seleccione un supervisor</option>

                                    @foreach ($supervisores as $supervisor)
                                    <option value="{!! $supervisor->id !!}" {!! old('supervisor') == $supervisor->id ? 'selected' : '' !!}>{!! $supervisor->nombre !!}</option>
                                    @endforeach

                                </select>

                                @error('supervisor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="rol">Formulario</label>
                                
                                <div class="input-group">
                                    <select class="form-control" id="formulario" name="formulario" required>
                                        <option value="" selected>Seleccione un formulario</option>

                                        @foreach ($formularios as $formulario)
                                        <option value="{!! $formulario->id !!}" {!! old('formulario') == $formulario->id ? 'selected' : '' !!}>{!! $formulario->nombre !!}</option>
                                        @endforeach

                                    </select>

                                    <div class="input-group-append">
                                        <button class="btn btn-sm btn-primary" id="mostrar_formulario" type="button">Ver</button>
                                    </div>
                                </div>

                                @error('formulario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mt-2 mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="button" class="btn btn-primary submit_class">
                                Crear
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://formbuilder.online/assets/js/form-builder.min.js"></script>
<script>

    jQuery(function($) {
        $('#mostrar_formulario').on('click', function(event) {
            let formulario = $('#formulario');
            let formulario_id = formulario.val();

            if (formulario_id == '') {
                Swal.fire(
                  'Cuidado!',
                  'Para obtener una vista previa debes elegir un formulario!',
                  'warning'
                  );

                return false;
            }

            let _url = '{{ route('formulario_cuerpo','xxxx') }}';
            let url = _url.replace('xxxx', formulario_id);

            $.ajax({
                url: url,
                type: 'GET',
            })
            .done(function(res) {
                console.log(res);
            });
            
        });
    });

</script>
@endsection