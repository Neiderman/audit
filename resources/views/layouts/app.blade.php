<!DOCTYPE html>
<html lang="es">

<head>
    <title>Auditorias - @yield('titulo')</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ asset('admin/plugins/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/base/vendor.bundle.base.css') }}">        
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables.net-bs4/dataTables.bootstrap4.css') }}">        
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">

    <link rel="shortcut icon" href="{{ asset('admin/images/favicon.png') }}" />
    @yield('styles')
</head>
<body>
    <div class="container-scroller">
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="navbar-brand-wrapper d-flex justify-content-center">
                <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">  
                    <a class="navbar-brand brand-logo" href="{{ route('home') }}">
                        {{-- <img src="{{ asset('admin/images/logo.svg') }}" alt="logo"/> --}}
                        Auditorias
                    </a>

                    <a class="navbar-brand brand-logo-mini" href="{{ route('home') }}">
                        {{-- <img src="{{ asset('admin/images/logo-mini.svg') }}" alt="logo"/> --}}
                        A
                    </a>

                    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                        <span class="mdi mdi-sort-variant"></span>
                    </button>
                </div>  
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            {{-- <img src="{{ asset('admin/images/faces/face5.jpg') }}" alt="profile"/> --}}
                            <span class="nav-profile-name">{!! Auth::user()->nombre !!}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                            
                            @if(Auth::user()->inv_activa == 'no')
                            <a class="dropdown-item" href="{{ route('perfil') }}">
                                <i class="mdi mdi-settings text-primary"></i>
                                Mi perfil
                            </a>
                            @endif

                            <a href="{{ route('salir') }}" class="dropdown-item">
                                <i class="mdi mdi-logout text-primary"></i>
                                Cerrar sesión
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>
        <div class="container-fluid page-body-wrapper">
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('home') }}">
                            <i class="mdi mdi-home menu-icon"></i>
                            <span class="menu-title">Inicio</span>
                        </a>
                    </li>

                    @if(Auth::user()->rol->nombre == 'Super Administrador')
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('empresas.index') }}">
                            <i class="mdi mdi-desktop-tower menu-icon"></i>
                            <span class="menu-title">Empresas</span>
                        </a>
                    </li>
                    @endif

                    @if(Auth::user()->rol->nombre == 'Super Administrador' || Auth::user()->rol->nombre == 'Administrador')
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('usuarios.index') }}">
                            <i class="mdi mdi-account-multiple menu-icon"></i>
                            <span class="menu-title">Usuarios</span>
                        </a>
                    </li>
                    @endif


                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('formularios.index') }}">
                            <i class="mdi mdi-file-document menu-icon"></i>
                            <span class="menu-title">Formularios</span>
                        </a>
                    </li>
                   
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('proyectos.index') }}">
                            <i class="mdi mdi-folder-multiple-outline menu-icon"></i>
                            <span class="menu-title">Proyectos</span>
                        </a>
                    </li>
                    
                </ul>
            </nav>

            <div class="main-panel">
                <div class="content-wrapper">

                    @yield('contenido')
                </div>

                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © {{ date('Y') }}. All rights reserved.</span>
                    </div>
                </footer>
            </div>

        </div>
    </div>

    <script src="{{ asset('admin/plugins/base/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('admin/plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('admin/js/off-canvas.js') }}"></script>
    <script src="{{ asset('admin/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('admin/js/template.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    
    @include('layouts.nswal')

    <script src="{{ asset('admin/js/dashboard.js') }}"></script>
    <script src="{{ asset('admin/js/data-table.js') }}"></script>
    <script src="{{ asset('admin/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/js/dataTables.bootstrap4.js') }}"></script>
    @yield('scripts')
</body>

</html>

