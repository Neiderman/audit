@extends('layouts.app')

@section('titulo')
Formularios
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor">&nbsp;/&nbsp;</i></a>
                    <p class="text-primary mb-0 hover-cursor">Formularios</p>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('formularios.create') }}';" class="btn btn-primary mt-2 mt-xl-0">Crear formulario</button>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title">Listado de formularios</p>
                <div class="table-responsive">
                    <table id="recent-purchases-listing" class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Fecha de creación</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($formularios as $formulario)
                            <tr>
                                <td>{!! $formulario->nombre !!}</td>
                                <td>{!! $formulario->created_at->format('d-m-Y') !!}</td>
                                <td>
                                    {!! $formulario->activo == 'si' ? 'Activo' : 'Inactivo' !!}
                                </td>
                                <td>
                                    <div class="btn-group col-sm">

                                        <button class="btn btn-xs btn-primary" onclick="location.href='{{ route('usuario_estado',$formulario->id) }}';" title="Editar formulario"><i class="mdi mdi-pencil"></i></button>
                                        
                                        @if ($formulario->activo == 'no')
                                        
                                        {{----}}<button class="btn btn-xs btn-success" onclick="location.href='{{ route('usuario_estado',$formulario->id) }}';" title="Habilitar formulario"><i class="mdi mdi-check"></i></button>
                                        
                                        @else
                                        
                                        {{----}}<button class="btn btn-xs btn-danger" onclick="location.href='{{ route('usuario_estado',$formulario->id) }}';" title="Inhabilitar formulario"><i class="mdi mdi-close"></i></button>
                                        
                                        @endif

                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#recent-purchases-listing').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });
    });
</script>
@endsection
