@extends('layouts.app')

@section('titulo')
Crear formulario
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('usuarios.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Formularios&nbsp;/&nbsp;</p></a>
                    <p class="text-primary mb-0 hover-cursor">Crear formulario</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form id="form_forms" method="POST" action="{{ route('formularios.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre" class="col-form-label text-md-right">{{ __('Name') }}</label>
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="estado" class="col-form-label text-md-right">Estado</label>
                                <select id="estado" type="text" class="form-control @error('estado') is-invalid @enderror" name="estado" required autofocus>
                                    <option value="no">Inactivo</option>
                                    <option value="si" selected>Activo</option>
                                </select>

                                @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div id="fb-editor"></div>
                    <hr>

                    <div class="form-group row mt-2 mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="button" class="btn btn-primary submit_class">
                                Crear
                            </button>
                        </div>
                    </div>

                    <input type="hidden" name="arreglos_campos" id="arreglos_campos">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://formbuilder.online/assets/js/form-builder.min.js"></script>
<script>

    jQuery(function($) {
        let options = {
            showActionButtons: false,
        };

        let fb_ed = $(document.getElementById('fb-editor')).formBuilder(options);

        setTimeout(function() {
            fb_ed.actions.setLang('es-ES');
        }, 1000);

        $('.submit_class').on('click', function(event) {
            $("#arreglos_campos").val(JSON.stringify(fb_ed.actions.getData()));
            setTimeout(function() {
                $("#form_forms").submit();
            }, 200);
        });
    });

</script>
@endsection