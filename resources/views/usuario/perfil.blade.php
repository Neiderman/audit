@extends('layouts.app')

@section('titulo')
Perfil
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Usuarios&nbsp;/&nbsp;</p>
                    <p class="text-primary mb-0 hover-cursor">Perfil</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Información personal</h4>
                <p class="card-description">
                    Aqui se gestiona tu información basica.
                </p>
                <form method="POST" action="{{ route('update_perfil',$usuario->id) }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="usuario">Nombre de usuario</label>
                                <input type="text" class="form-control @error('usuario') is-invalid @enderror" id="usuario" name="usuario" value="{!! $usuario->usuario !!}" placeholder="Nombre de usuario">
                                
                                @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre">Nombre completo</label>
                                <input type="text" class="form-control @error('nombre') is-invalid @enderror" id="nombre" name="nombre" value="{!! $usuario->nombre !!}" placeholder="Nombre completo">

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Correo electronico</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{!! $usuario->email !!}" placeholder="Correo electronico">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <hr>

                    <p class="card-description">
                        Si no desea actualizar la contraseña, deje en blanco ambos campos.
                    </p>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" autocomplete="new-password">
                        
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Confirm Password</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Contraseña" autocomplete="new-password">
                    </div>

                    <div class="btn-group">
                        <button type="button" onclick="location.href='{{ route('home') }}';" class="btn btn-secondary">Volver al inicio</button>
                        <button type="submit" class="btn btn-primary">Actualizar información</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
