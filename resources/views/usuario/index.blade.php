@extends('layouts.app')

@section('titulo')
Usuarios
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor">&nbsp;/&nbsp;</i></a>
                    <p class="text-primary mb-0 hover-cursor">Usuarios</p>
                </div>
            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">
                <button type="button" onclick="location.href='{{ route('usuarios.create') }}';" class="btn btn-primary mt-2 mt-xl-0">Crear usuario</button>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <p class="card-title">Listado de usuarios</p>
                <div class="table-responsive">
                    <table id="recent-purchases-listing" class="table">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Nombre</th>
                                <th>Correo electronico</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($usuarios as $usuario)
                            <tr>
                                <td>{!! $usuario->usuario !!}</td>
                                <td>{!! $usuario->nombre !!}</td>
                                <td>{!! $usuario->email !!}</td>
                                <td>
                                    @if ($usuario->inv_activa == 'no')
                                    {!! $usuario->activo == 'si' ? 'Activo' : 'Inactivo' !!}
                                    @else
                                    Inactivo
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group col-sm">

                                        @if ($usuario->inv_activa == 'no')
                                        {{----}}<button class="btn btn-xs btn-info" type="button" onclick="location.href='{{ route('usuarios.edit',$usuario->id) }}';" title="Editar empresa."><i class="mdi mdi-grease-pencil"></i></button>

                                        {{----}}@if (Auth::user()->id != $usuario->id)
                                        {{----}}{{----}}@if ($usuario->activo == 'no')
                                        
                                        {{----}}{{----}}{{----}}<button class="btn btn-xs btn-success" onclick="location.href='{{ route('usuario_estado',$usuario->id) }}';" title="Habilitar empresa"><i class="mdi mdi-check"></i></button>
                                        
                                        {{----}}{{----}}@else
                                        
                                        {{----}}{{----}}{{----}}<button class="btn btn-xs btn-danger" onclick="location.href='{{ route('usuario_estado',$usuario->id) }}';" title="Inhabilitar empresa"><i class="mdi mdi-close"></i></button>
                                        
                                        {{----}}{{----}}@endif
                                        {{----}}@endif
                                        
                                        @else

                                        {{----}}<button class="btn btn-xs btn-warning" onclick="location.href='{{ route('enviar_invitacion',$usuario->id) }}';" title="Reenviar invitación al correo"><i class="mdi mdi-email"></i></button>

                                        @endif

                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#recent-purchases-listing').DataTable({
            "language": {
                search: "Buscar"
            },
            paging: false, info: false
        });
    });
</script>
@endsection
