@extends('layouts.app')

@section('titulo')
Crear usuario
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('usuarios.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Usuarios&nbsp;/&nbsp;</p></a>
                    <p class="text-primary mb-0 hover-cursor">Crear usuario</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('usuarios.store') }}">
                    @csrf

                    <h4 class="card-title">Tipo de registro y rol usuario</h4>

                    @if(Auth::user()->rol->nombre == 'Super Administrador' || Auth::user()->rol->nombre == 'Administrador')
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-check">
                                <label class="form-check-label text-muted">
                                    <input class="form-check-input" type="checkbox" name="_invitacion" id="_invitacion" {{ old('_invitacion') ? 'checked' : '' }}>
                                    Modo invitación por correo
                                </label>
                                <input type="hidden" name="inv_activa" id="inv_activa" value="no">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="rol">Rol</label>
                                <select class="form-control" id="rol" name="rol" required>
                                    <option value="" selected>Seleccione un rol</option>

                                    @foreach ($roles as $rol)
                                    
                                    @if(Auth::user()->rol->nombre == "Administrador" && ($rol->nombre == "Super Administrador" || $rol->nombre == "Administrador"))
                                    @continue
                                    @endif

                                    <option value="{!! $rol->id !!}" {!! old('rol') == $rol->id ? 'selected' : '' !!}>{!! $rol->nombre !!}</option>
                                    @endforeach

                                </select>

                                @error('rol')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="col-md-4 div_empresa" style="display: none;">
                            <div class="form-group">
                                <label for="empresa">Empresa</label>
                                <div class="input-group">
                                    <select class="form-control" id="empresa" name="empresa">
                                        <option value="" selected>Seleccione una empresa</option>

                                        @foreach ($empresas as $empresa)

                                        <option value="{!! $empresa->id !!}" {!! old('empresa') == $empresa->id ? 'selected' : '' !!}>{!! $empresa->nombre !!}</option>

                                        @endforeach

                                    </select>
                                    <div class="input-group-append">
                                        <button  onclick="window.open('{{ route('empresas.create') }}','_blank');" target="_blank" class="btn btn-sm btn-primary" type="button"><i class="mdi mdi-plus"></i></button>
                                    </div>
                                </div>

                                @error('empresa')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <hr>

                    @endif

                    <h4 class="card-title">Información personal</h4>
                    <p class="card-description">
                        Aqui se gestiona tu información basica.
                    </p>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="usuario">Nombre de usuario</label>
                                <input type="text" class="form-control @error('usuario') is-invalid @enderror" id="usuario" name="usuario" value="{!! old('usuario') !!}" placeholder="Nombre de usuario">

                                @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre">Nombre completo</label>
                                <input type="text" class="form-control @error('nombre') is-invalid @enderror" id="nombre" name="nombre" value="{!! old('nombre') !!}" required placeholder="Nombre completo">

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Correo electronico</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{!! old('email') !!}" required placeholder="Correo electronico">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="seccion_password">

                        <hr>

                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirmar Contraseña</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Contraseña" autocomplete="new-password">
                        </div>
                    </div>

                    <div class="btn-group">
                        <button type="button" onclick="location.href='{{ route('usuarios.index') }}';" class="btn btn-secondary">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Crear usuario</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#rol').on('change', function(event) {
            let roles = $(this).find('option');
            $.each(roles, function(index, val) {
                if ($(val).is(':checked')) {
                    let rol = $(val).html();
                    if (rol == "Administrador") {
                        $('.div_empresa').show();
                    } else {
                        $('.div_empresa').hide();
                    }
                }
            });
        });

        $('#_invitacion').on('change', function(event) {
            let check = $(this);
            let checkeado = "si";

            if (!check.is(':checked')) {

                $('#usuario').parents('div.col-md-6').show();
                $('div.seccion_password').show();
                checkeado = "no";

            } else {

                $('#usuario').parents('div.col-md-6').hide();
                $('div.seccion_password').hide();
                checkeado = "si";
            }

            $('#inv_activa').val(checkeado)
        });

        $('#_invitacion').change();
        $('#rol').change();
        $('#empresa').change();


    });
</script>
@endsection