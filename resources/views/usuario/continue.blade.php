@extends('layouts.app')

@section('titulo')
Completar registro
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="d-flex">
                    <a href="{{ route('home') }}"><i class="mdi mdi-home text-muted hover-cursor"></i></a>
                    <a href="{{ route('usuarios.index') }}"><p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Usuarios&nbsp;/&nbsp;</p></a>
                    <p class="text-primary mb-0 hover-cursor">Completar registro</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form id="form_forms" method="POST" action="{{ route('validar_update',$usuario->id) }}">
                    @csrf

                    <input type="hidden" value="no" id="inv_activa" name="inv_activa">

                    <h4 class="card-title">Información personal</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Correo electronico</label>
                                <input type="email" class="form-control" readonly value="{!! $usuario->email !!}" placeholder="Correo electronico">
                            </div>
                        </div>
                        
                        @if ($usuario->empresa)
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="empresa">Empresa</label>
                                <input type="empresa" class="form-control" readonly value="{!! $usuario->empresa->nombre !!}" placeholder="Correo electronico">
                            </div>
                        </div>
                        
                        @endif

                    </div>

                    <hr>
                    <p class="card-description">
                        Completa la información restante y define tu contraseña.
                    </p>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="usuario">Nombre de usuario</label>
                                <input type="text" class="form-control @error('usuario') is-invalid @enderror" id="usuario" name="usuario" value="{!! $usuario->usuario !!}" placeholder="Nombre de usuario">

                                @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre">Nombre completo</label>
                                <input type="text" class="form-control @error('nombre') is-invalid @enderror" id="nombre" name="nombre" value="{!! $usuario->nombre !!}" required placeholder="Nombre completo">

                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password_confirmation">Confirmar Contraseña</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Contraseña" autocomplete="new-password">
                            </div>
                        </div>
                    </div>

                    <div class="btn-group">
                        <button type="button" onclick="location.href='{{ route('usuarios.index') }}';" class="btn btn-secondary">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Finalizar registro</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection